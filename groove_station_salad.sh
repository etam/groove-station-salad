#!/bin/bash

# Thanks to http://people.skolelinux.org/pere/blog/Simple_streaming_the_Linux_desktop_to_Kodi_using_GStreamer_and_RTP.html

somafm_src="http://ice6.somafm.com/groovesalad-128-mp3"
youtube_src="https://www.youtube.com/watch?v=fR2qcCl_JB4"
kodihost="user:pass@localhost:8080"

youtube_hls_src="$(youtube-dl --get-url "$youtube_src")" || exit $?


kodicmd() {
    host="$1"
    cmd="$2"
    params="$3"
    curl -X POST --silent --header 'Content-Type: application/json' \
        --data-binary "{ \"id\": 1, \"jsonrpc\": \"2.0\", \"method\": \"${cmd}\", \"params\": ${params} }" \
        "http://${host}/jsonrpc"
}

cleanup() {
    # Stop the playing when we end
    playerid="$(kodicmd "$kodihost" Player.GetActivePlayers "{}" | jq .result[].playerid)"
    if [[ -n "$playerid" ]]; then
        kodicmd "$kodihost" Player.Stop "{ \"playerid\" : $playerid }" > /dev/null
    fi
    if [[ -n "$gstpid" ]] && kill -0 "$gstpid" >/dev/null 2>&1; then
        kill "$gstpid"
    fi
}
trap cleanup EXIT INT


stream_host=localhost
stream_port=1234

# No reencoding here! Only demux and mux.
gst-launch-1.0 \
    mpegtsmux name=mux ! tcpserversink host="$stream_host" port="$stream_port" \
    souphttpsrc iradio-mode=true is-live=true location="$somafm_src" ! icydemux ! audio/mpeg ! mpegaudioparse ! queue2 ! mux. \
    souphttpsrc is-live=true location="$youtube_hls_src" ! hlsdemux ! tsdemux ! video/x-h264 ! queue2 ! mux. \
    >/dev/null 2>&1 &
gstpid="$!"

# Give stream a second to get going
sleep 3

# Ask kodi to start streaming using its JSON-RPC API
kodicmd "$kodihost" Player.Open \
    "{\"item\": { \"file\": \"tcp://@${stream_host}:${stream_port}\" } }" > /dev/null

# wait for gst to end
wait "$gstpid"
